<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<form action="<?php echo SITE_URL;?>shop/addshop" id="form" method="post">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Registration</h3>
				
				<div class="form-group col-lg-12">
					<label>Shop Name*</label>
					<input type="" name="name" class="form-control" id="" value="">
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="form-group col-lg-6">
					<label>Select Route</label>
					<select>
						<option></option>
					</select>
				</div>

				<div class="form-group col-lg-12">
					<label>Mobile No</label>
					<input type="" name="mobile" class="form-control" id="" value="">
					
				</div>

				<div class="form-group col-lg-12">
					<label>Contact No*</label>
					<input type="" name="contact_no" class="form-control" id="" value="">
					<?php echo form_error('contact_no'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>Landmark</label>
					<input type="" name="landmark" class="form-control" value="">
					
				</div>
				
				<div class="form-group col-lg-6">
					<label>Address*</label>
					<textarea rows="5" cols="100" name="address">
					</textarea>
					<?php echo form_error('address'); ?>
				</div>
								
				<div class="form-group col-lg-6">
					<label>city*</label>
					<input type="" name="city" class="form-control" id="" value="">
					<?php echo form_error('city'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>District</label>
					<input type="" name="district" class="form-control" id="" value="">
				</div>

				<div class="form-group col-lg-6">
					<label>Tin No</label>
					<input type="" name="tin_no" class="form-control" id="" value="">
				</div>

				<div class="form-group col-lg-6">
					<label>Drug License No</label>
					<input type="" name="city" class="form-control" id="" value="">
				</div>

				<div class="form-group col-lg-6">
					<label>Notes</label>
					<textarea rows="5" cols="100" name="notes">
					</textarea>
				</div>
			
			</div>
		
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">create</button>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="view" value="1">
</form>

<script>
/*
$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
	
       // Specify the validation rules
       rules: {
           name: {
               required: true
           },
		   mobile: {
               required: true,
			   minlength: 10,
			   number:true
			   
           },
           password: {
               required: true,
               minlength: 5
           },
			confpassword: {
               required: true,
			   minlength: 5,
               equalTo: '#password'
           },
			email:
		   {
				email:true
		   }

       },
       
       // Specify the validation error messages
       messages: {
		   mobile: {
               required: "Please provide a mobile no",
               minlength: "Your mobile no must be at least 10 nos long",
			   number:"please enter valid mobile no"
           },
           password: {
               required: "Please provide a password",
               minlength: "Your password must be at least 5 characters long"
           },
			
           name: "Please provide a name"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });
*/
</script>