<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/addproduct_validation.js"></script>
<!-- validation js -->


<form action="<?php echo SITE_URL;?>product/addproduct" id="form" method="post" enctype="multipart/form-data">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Add new product</h3>
				
				<div class="form-group col-lg-12">
					<label>Product Name*</label>
					<input type="" name="name" class="form-control" id="name" value="">
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="form-group col-lg-6">
				
					<label>Product Category*:</label>
					<select name="prodcategory" id="prodcategory">
						
						<?php 

						$prod_category = $controller->getprodcategory_get('1');//passing 1 for recognizing whether web view

							foreach($prod_category as $prod_category_list):
						?>
						<option value="<?php echo $prod_category_list['prod_cat_id'];?>"><?php echo $prod_category_list['prod_cat_name'];?></option>
						<?php endforeach; ?>
					<select>
					<?php echo form_error('prodcategory'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>Select unit*:</label>
					<select name="unit" id="unit">
							
						<?php 

						$unit = $controller->getunit_get('1');//passing 1 for recognizing whether web view

							foreach($unit as $unit_list):
						?>
						<option value="<?php echo $unit_list['unit_id'];?>"><?php echo $unit_list['unit_name'];?></option>
						<?php endforeach; ?>
						
					<select>
					<?php echo form_error('unit'); ?>
				</div>

				<div class="form-group col-lg-12">
					<label>Unit per price*</label>
					<input type="" name="unit_per_price" class="form-control" id="unit_per_price" value="">
					<?php echo form_error('unit_per_price'); ?>
				</div>

				<div class="form-group col-lg-12">
					<label>Qunatity per pack*</label>
					<input type="" name="qty_per_pack" class="form-control" id="qty_per_pack" value="">
					<?php echo form_error('qty_per_pack'); ?>
				</div>
				
				<div class="form-group col-lg-6">
					<label>Description</label>
					<textarea rows="5" cols="100" name="description" id="description">
					</textarea>
					
				</div>

				<div class="form-group col-lg-6">
					<label>Other info</label>
					<textarea rows="5" cols="100" name="other_info" id="other_info">
					</textarea>
					
				</div>
								
				<div class="form-group col-lg-12">
					<label>Product Image:</label>
					<input type="file" name="photo" class="form-control" id="photo" value="">
				</div>
			
			</div>
		
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">create</button>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="view" value="1">
</form>