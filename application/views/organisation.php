<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>


<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/addorganisation_validation.js"></script>
<!-- validation js -->

<form action="<?php echo SITE_URL;?>organisation/addorganisation" id="form" method="post" enctype="multipart/form-data">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Create New Organisation</h3>
				
				<div class="form-group col-lg-12">
					<label>Organisation Name*</label>
					<input type="" name="name" class="form-control" id="name" value="">
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="form-group col-lg-12">
					<label>Organisation Description</label>
					<textarea rows="5" cols="60" name="organisation_description" id="organisation_description">
					</textarea>
				</div>
			
			</div>
		
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">create</button>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="view" value="1">
</form>