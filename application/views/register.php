<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/register_validation.js"></script>
<!-- validation js -->


<form action="<?php echo SITE_URL;?>register/doregister" id="form" method="post" enctype="multipart/form-data">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Registration</h3>
				
				<div class="form-group col-lg-12">
					<label>Mob No*</label>
					<input type="" name="mobile" class="form-control" id="mobile" value="">
					<?php echo form_error('mobile'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>Password*</label>
					<input type="password" name="password" class="form-control" id="password" value="">
					<?php echo form_error('password'); ?>
				</div>
				
				<div class="form-group col-lg-6">
					<label>Company Name*</label>
					<input type="" name="company" id="company" class="form-control" value="">
					<?php echo form_error('company'); ?>
				</div>
				
			</div>
			<?php if(isset($response)) : echo $response; endif; ?>
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">Register</button>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="view" value="1">
</form>