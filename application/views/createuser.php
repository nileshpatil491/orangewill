<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>


<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/adduser_validation.js"></script>
<!-- validation js -->

<link rel="stylesheet" href="<?php echo SITE_URL;?>docsupport/prism.css">
<link rel="stylesheet" href="<?php echo SITE_URL;?>docsupport/chosen.css">

<script src="<?php echo SITE_URL;?>docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>docsupport/chosen.jquery.js" type="text/javascript"></script>

<form action="<?php echo SITE_URL;?>user/createuser" id="form" method="post" enctype="multipart/form-data">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Create New User</h3>
				
				<div class="form-group col-lg-12">
					<label>Name*</label>
					<input type="" name="name" class="form-control" id="name" value="">
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="form-group col-lg-12">
					<label>Mob No*</label>
					<input type="" name="mobile" class="form-control" id="mobile" value="">
					<?php echo form_error('mobile'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>Organisation</label>
					
					<select name="organisation[]" size="2" multiple="multiple" id="organisation" class="chosen-select">
						<?php 
								
							$orglist = $controller->getorg_get('1');//passing 1 for recognizing whether web view

							foreach($orglist as $org):
						?>
						<option value="<?php echo $org['org_id'];?>" ><?php echo $org['org_name'];?></option>
						<?php endforeach; ?>
					<select>
				</div>
								
				<div class="form-group col-lg-6">
					<label>Email Address</label>
					<input type="" name="email" class="form-control" id="email" value="">
					<?php echo form_error('email'); ?>
				</div>

				<div class="form-group col-lg-6">
					<label>User Type</label>
					<select name="usertype" id="usertype">
						<?php 
							$usr_type = $controller->getusrtyp_get('1');//passing 1 for recognizing whether web view

							foreach($usr_type as $usrtyp):
						?>
						<option value="<?php echo $usrtyp['user_type_id'];?>" <?php if($usrtyp['user_type_id']==3): echo 'selected'; endif; ?>><?php echo $usrtyp['user_type_name'];?></option>
						<?php endforeach; ?>
					<select>
					
				</div>
				
				<div class="form-group col-lg-6">
					<label>Select route</label>
					
					<select name="route[]" size="2" multiple="multiple" id="route">
						
					<select>
				</div>

				<div class="form-group col-lg-12">
					<label>Photo</label>
					<input type="file" name="user_photo" class="form-control" id="photo" value="">
				</div>
			
			</div>
		
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">create user</button>
			</div>
		</div>
	</section>
</div>
<?php 
	print_r((isset($message))?$message:'');
?>
<input type="hidden" name="view" value="1">
</form>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<script type="text/javascript">
$("#organisation").change(function(){

		if($('#usertype').val()=='1')
		{
			autofill($(this).val());
		}
		else
		{
					$('#route option').each(function(index, option) {
						$(option).remove();
					});

					/* auto fill select */
					$('#route').attr('enabled', 'false');
		}
	

	});

	$('#usertype').change(function(){
		
		if($('#usertype').val() == '1')
		{
			autofill($('#organisation').val());
		}
		else
		{
			$('#route option').each(function(index, option) {
						$(option).remove();
					});

					/* auto fill select */
					$('#route').attr('enabled', 'false');
		}
		
	});

	function autofill(org_id)
	{
	
		 $.ajax({url: "<?php echo SITE_URL?>user/getroute",
				 
				 type: "GET",
				 data: {org_id : org_id,view : 1},
					
				  success: function(response){
					
					var json_obj = $.parseJSON(response);//parse JSON	

					$('#route option').each(function(index, option) {
						$(option).remove();
					});

					/* auto fill select */
					$('#route').attr('enabled', 'true');
					  $.each(json_obj,function(index,item){
							
						
							$("#route").append($("<option></option>").attr("value", item.route_id).text(item.route_name));
							
						 

					});

					/* auto fill select */

			}});
		
	}

</script>