<!--route modal-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>

<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/addroute_validation.js"></script>
<!-- validation js -->


<div id="routeModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <h1 class="text-center">route</h1>
      </div>
      <div class="modal-body">
	 
          <form action="<?php echo SITE_URL;?>route/addroute" name="frmroute" method="post" id="form">
            <div class="form-group col-lg-12">
					<label>Route Name*</label>
					<input type="" name="route" class="form-control" id="route" value="">
					<?php echo form_error('route'); ?>
				</div>
				
				<div class="form-group col-lg-6">
					<label>Description</label>
					<textarea rows="5" cols="70" name="description" id="description">
					</textarea>
				</div>

				<div class="form-group col-lg-12">
					<label>Organisation</label>
					<select name="organisation" id="organisation">
							<?php 
							
								$orglist = $controller->getorglist_get('1');//passing 1 for recognizing whether web view
							
								foreach($orglist as $org):
							?>
							<option value="<?php echo $org['org_id'];?>" ><?php echo $org['org_name'];?></option>
							<?php endforeach; ?>
					<select>
				</div>
				<div class="modal-footer">
				  <input type="submit" value="create">
			  </div>
				
				<input type="hidden" name="view" value="1">
          </form>
      </div>

	  
      
  </div>
  </div>
</div>