<!--dashboard modal-->

<div id="dashboardModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <h1 class="text-center">dashboard</h1>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
          
		<a href="<?php echo SITE_URL;?>organisation">
			Organisation
		</a>|
		<a href="<?php echo SITE_URL;?>shop">
			Shop
		</a>|
		<a href="<?php echo SITE_URL;?>route">
			Routes
		</a>|
		<a href="<?php echo SITE_URL;?>user">
			Users
		</a>|
		<a href="">
			Distributors
		</a>|
		<a href="<?php echo SITE_URL;?>product">
			Products
		</a>|
		<a href="<?php echo SITE_URL;?>prodcategory">
			Products Category
		</a>|
		<a href="">
			Salesman Activity
		</a>|
		<a href="">
			Company
		</a>|
		<a href="">
			Analytics
		</a>|
		<a href="<?php echo SITE_URL;?>logout">
			Logout
		</a>


      </div>
  </div>
  </div>
</div>