<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<!-- validation js -->
<script src="<?php echo base_url();?>dist/js/addprodcategory_validation.js"></script>
<!-- validation js -->


<link rel="stylesheet" href="<?php echo SITE_URL;?>docsupport/prism.css">
<link rel="stylesheet" href="<?php echo SITE_URL;?>docsupport/chosen.css">

<script src="<?php echo SITE_URL;?>docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo SITE_URL;?>docsupport/chosen.jquery.js" type="text/javascript"></script>

<form action="<?php echo SITE_URL;?>prodcategory/addprodcategory" id="form" method="post" enctype="multipart/form-data">
<div class="container-fluid">
    <section class="container">
		<div class="container-page">				
			<div class="col-md-6">
				<h3 class="dark-grey">Create New Category</h3>
				
				<div class="form-group col-lg-12">
					<label>Category Name*</label>
					<input type="" name="name" class="form-control" id="name" value="">
					<?php echo form_error('name'); ?>
				</div>
				
				<div class="form-group col-lg-12">
					<label>Category Description</label>
					<textarea rows="5" cols="60" name="prodcategory_description" id="prodcategory_description">
					</textarea>
				</div>

				<div class="form-group col-lg-12">
					<label>Organisation</label>
					<select name="organisation[]" class="chosen-select" size="2" multiple="multiple" id="organisation">
							<?php 
								$orglist = $controller->getorglist_get('1');//passing 1 for recognizing whether web view
							
								foreach($orglist as $org):
							?>
							<option value="<?php echo $org['org_id'];?>" ><?php echo $org['org_name'];?></option>
							<?php endforeach; ?>
					<select>
				</div>
			
				<?php echo (isset($response))?$response:''; ?>
			</div>
		
			<div class="col-md-6">
				<button type="submit" class="btn btn-primary">create</button>
			</div>
		</div>
	</section>
</div>
<input type="hidden" name="view" value="1">
</form>
<script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
