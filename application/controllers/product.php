<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Product extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('product/productmodel');
		$this->load->model('unit/unitmodel');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('token/tokenmodel');
	}


	public function index_get()
	{
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{	
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				
					if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
					{
						$object['controller'] = $this;
						$this->load->view('product',$object);
					}
					else
					{
						redirect('dashboard','refresh');
					}
			}
			else
			{
				redirect('login','refresh');
			}
		}
		else
		{
				redirect('login','refresh');
		}
	}

	public function addproduct_post()
	{

		$view = $this->input->post('view');
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
					{

						$object['controller'] = $this;

						/* server side validation */
						$this->form_validation->set_rules('name', 'product name', 'required');
						$this->form_validation->set_rules('prodcategory', 'Product category', 'required');
						$this->form_validation->set_rules('unit', 'product unit', 'required');
						$this->form_validation->set_rules('unit_per_price', 'Product unit per price', 'required|numeric');
						$this->form_validation->set_rules('qty_per_pack', 'Product quantity per pack', 'required|numeric');
									if ($this->form_validation->run() == FALSE)
									{
										if($view!='')
										{
											$this->load->view('product',$object);
										}
										else
										{
											$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));

											$this->response($error,OK);
										}
									}
									else
									{
										$this->checkproduct();
									}

						/* server side validation */
					}
					else
					{
						if(isset($view))
						{
							redirect('dashboard','refresh');
						}
						else
						{
							$result_set['status']='Failed';
							$result_set['message'] = "Permission denied";

							return $this->response($result_set);
						}
					}	
			}
			else
			{

				if(isset($view))
				{
					redirect('login','refresh');
				}
				else
				{
					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				}
			}
		}
		else
		{
			if(isset($view))
			{
				redirect('login','refresh');
			}
			else
			{
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);
			}

		}

	}

	/* get product category */
	public function getprodcategory_get($id="null")
	{
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{

				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
				{

					$result = $this->productmodel->get_prodcategory();
						

					if($id!='null')
					{
						return $result;
					}
					else
					{
					
						$result_set['status']='success';
						$result_set['message'] = "";
						$result_set['product_category'] = $result;
						
						$this->response($result_set);
						
					}	
				}
				else
				{	
					$result_set['status']='Failed';
					$result_set['message'] = "Permission denied";

					return $this->response($result_set);
				}

				
			}
			else
			{

					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				
			}
		}
		else
		{
			
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);

		}
		
	}

	/* get unit list */
	public function getunit_get($id="null")
	{

		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{

				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
				{

					$result = $this->unitmodel->get_unit();

					
					if($id!='null')
					{
						return $result;
						
					}
					else
					{
					
						$result_set['status']='success';
						$result_set['message'] = "";
						$result_set['unit'] = $result;
						
						$this->response($result_set);
						
					}	

				}
				else
				{	
					$result_set['status']='Failed';
					$result_set['message'] = "Permission denied";

					return $this->response($result_set);
				}

				
			}
			else
			{

					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				
			}
		}
		else
		{
			
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);

		}

	}

	public function checkproduct()
		{
			$view = $this->input->post('view');
			/* setting up user id */
			$session_data = $this->session->userdata('usr_data'); /* retriving session data */

			$user_id = $session_data['user_id'];
				
				$prod_name = $this->input->post('name');
				$prodcategory = $this->input->post('prodcategory');
				$unit = $this->input->post('unit');
				$unit_per_price = $this->input->post('unit_per_price');
				$qty_per_pack = $this->input->post('qty_per_pack');
				$description = $this->input->post('description');
				$other_info = $this->input->post('other_info');
			
				/* product image upload */

							$config['upload_path']= './uploads/';
							$config['allowed_types'] = 'gif|jpg|png|jpeg';
							$config['max_size']	= '20000';
							$config['max_width']  = '1024';
							$config['max_height']  = '768'; 

							$this->load->library('upload', $config);
				/* product image upload */

							
									$data['img_name'] = $this->upload->data();	
									$data = array(
											   'prod_name'=>$prod_name,
												'description'=>$description,
												'user_id'=>$user_id,
												'prod_cat_id'=>$prodcategory,
												'unit_id' => $unit,
												'unit_per_price' => $unit_per_price,
												'qty_per_pack' => $qty_per_pack,
												'other_info'=>$other_info,
												'img_name'=>$data['img_name']['file_name'],
												);
									

									/* removing empty elements from data array */
									foreach($data as $key=>$value)
									{
										if($value=='null')
										{
											unset($data[$key]);
										}
									}

									/* checking product category already exist */

										$result = $this->productmodel->createProduct($data);
									
										if($result)
										{
											
											//$this->form_validation->set_message('username','username or password is incorrect');
											
											if($view!='')
											{
												redirect('dashboard'); 
												return true;
											}

											$fnresult['status'] = "success";
											$fnresult['message'] = "New product added successfully";
											$fnresult['product_id'] = $result;
											$this->response($fnresult,OK);	
											
										}
										else
										{	

											if($view!='')
											{
												$this->form_validation->set_message('checkcategory','something goes wrong');
												return false;
											}
											else
											{
												$fnresult['status']=='failed';
												$fnresult['message'] = "New product addition failed";
												$this->response($fnresult,NOT_FOUND);
											}
										}

				
			
		}

		public function addproduct_get()
		{
			redirect('product','refresh');
		}
	
}
