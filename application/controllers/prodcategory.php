<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Prodcategory extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	//var $object = array();	

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('product/Productmodel');
		$this->load->model('organisation/organisationmodel');
		$this->load->model('token/tokenmodel');
		$this->load->library('form_validation');
		$this->load->library('session');
		
	}

/*
	public function create_obj()
	{
		return new Prodcategory;
	}
*/

	public function index_get()
	{
		
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
				{
					$object['controller'] = $this;
					$this->load->view('product_category',$object);
				}
				else
				{
					redirect('dashboard','refresh');
				}
				
			}
			else
			{
				redirect('login','refresh');
			}
		}
		else
		{
				redirect('login','refresh');
		}
		
	}

	public function addprodcategory_post()
	{

		$view = $this->input->post('view');

		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{

			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{

					if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
					{
						$object['controller'] = $this;
						
						/* server side validation */
						$this->form_validation->set_rules('name', 'Product Category Name', 'required|callback_checkcategory');
									if ($this->form_validation->run() == FALSE)
									{

										if($view!='')
										{
											$this->load->view('product_category',$object);
										}
										else
										{
											$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));
											$this->response($error);
										}

										
										//$this->load->view('login');	
									}

						/* server side validation */	
					}
					else
					{
						if(isset($view))
						{
							redirect('dashboard','refresh');
						}
						else
						{
							$result_set['status']='Failed';
							$result_set['message'] = "Permission denied";

							return $this->response($result_set);
						}
					}		
			}
			else
			{

				if(isset($view))
				{
					redirect('login','refresh');
				}
				else
				{
					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				}
			}
		}
		else
		{
			if(isset($view))
			{
				redirect('login','refresh');
			}
			else
			{
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);
			}

		}

		

	}

	/* get organisation list */
	public function getorglist_get($id=null)
	{
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{

				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
				{
					$result = $this->organisationmodel->get_organisationlist();

					if($id!=null)
					{
						return $result;
					}
					else
					{
						
						$result_set['status']='success';
						$result_set['message'] = "";
						$result_set['organisation'] = $result;
						return $this->response($result_set);
						
					}	
				}
				else
				{	
					$result_set['status']='Failed';
					$result_set['message'] = "Permission denied";

					return $this->response($result_set);
				}

				
			}
			else
			{

					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				
			}
		}
		else
		{
			
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);

		}
				
	}

	public function checkcategory()
		{
			
		

		/* setting up user id */
		$session_data = $this->session->userdata('usr_data'); /* retriving session data */

		$user_id = $session_data['user_id'];
		$comp_id = $session_data['comp_id'];
			
			$prodcat_name = $this->input->post('name');
			$prodcat_description = $this->input->post('prodcategory_description');
			$organisation = $this->input->post('organisation');

			$view = $this->input->post('view');

			$data = array(
					   'prod_cat_name'=>$prodcat_name,
						'description'=>$prodcat_description,
						'added_by'=>$user_id,
						'org_id'=>json_encode($organisation),
						'comp_id' => $comp_id
						);
			

			/* removing empty elements from data array */
			foreach($data as $key=>$value)
			{
				if($value=='null')
				{
					unset($data[$key]);
				}
			}

			/* checking product category already exist */

			$resultcnt = $this->Productmodel->getCategory($prodcat_name,$comp_id); 


			if($resultcnt)
			{

				if($view!='')
				{
					$this->form_validation->set_message('checkcategory','Category already exist');
					return false;
				}
				else
				{
					$fnresult['status']='failed';
					$fnresult['message'] = "Category already exist";
					$this->response($fnresult);
				}
				
				
			}
			else
			{
				$result = $this->Productmodel->createProdcategory($data);
            
				if($result)
				{
					
					//$this->form_validation->set_message('username','username or password is incorrect');
					
					if($view!='')
					{
						redirect('dashboard'); 
						return true;
					}

					$fnresult['status'] = "success";
					$fnresult['message'] = "New category added successfully";
					$fnresult['category_id'] = $result;
					$this->response($fnresult,OK);	
					
				}
				else
				{	

					if($view!='')
					{
						$this->form_validation->set_message('checkcategory','something goes wrong');
						return false;
					}
					else
					{
						$fnresult['status']=='failed';
						$fnresult['message'] = "New category addition failed";
						$this->response($fnresult,NOT_FOUND);
					}
				}
			}


		}

		public function addprodcategory_get()
		{
			redirect('prodcategory','refresh');
		}
	
}
