
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Register extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();

		$this->load->model('register/Sregister');
		$this->load->model('user/usermodel');
		$this->load->library('form_validation');
		$this->load->library('session');
	}


	public function index_get()
	{
		$this->load->view('register');
	}

	/* new user registration */
	public function doregister_post()
	{
		$view = $this->input->post('view');

		/* server side validation */
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric|min_length[10]|max_length[12]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('company', 'Company', 'required');

					if ($this->form_validation->run() == FALSE)
					{

						if($view!='')
						{
							$this->load->view('register');
						}
						else
						{
							$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));

							$this->response($error,OK);
						}
					}
					else
					{
						$this->checkreg();
					}
		/* server side validation */
		
	}

	
	public function checkreg()
	{
			$mob = $this->input->post('mobile');
            $passwd = md5($this->input->post('password'));
			$company = $this->input->post('company');

			$view = $this->input->post('view');
            $data = array(
					'mob_no'=>$mob,
                    'passwd'=>$passwd,
					'company'=>$company
					 );
          		
			/* checking mobile no already exist */

			$resultcnt = $this->usermodel->getMob($mob); 

			if($resultcnt)
			{

				if($view!='')
				{
					$response['response'] = 'mobile no already exist';
					$this->load->view('register',$response);
					
				}
				else
				{
					$fnresult['status']='failed';
					$fnresult['message'] = "mobile no already exist";
					$this->response($fnresult);
				}
				
				
			}
			else
			{
				$result = $this->Sregister->create($data);    
            
				if($result)
				{
					
					//$this->form_validation->set_message('username','username or password is incorrect');
					

					/* email after successfull registration 

					
					$message='Your account has been successfully created.<br/>Please click on below URL or paste into your browser<br/><br/>	 
					<br/>Login Information<br/>User ID:'.'													'.$mob.'<br/>Password:'.$passwd.'<br/>Thanks<br/>Admin Team';

						$this->sendmail($credentials['email'],$message);



					*/



					if($view!='')
					{
						redirect('login','refresh');
						return true;
					}

					$fnresult['status'] = "success";
					$fnresult['message'] = "register success";
					$fnresult['user_id'] = $result;
					
					$this->response($fnresult);	
					
				}
				else
				{	

					if($view!='')
					{
						
						redirect('register'); 
						return false;
					}
					else
					{
						$fnresult['status']=='failed';
						$fnresult['message'] = "Registration failed";
						$this->response($result);
					}
				}
			}

			/* checking mobile no already exist */
	
	}

	public function doregister_get()
	{
		redirect('register','refresh'); 
	}

	
}
