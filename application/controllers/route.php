<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Route extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		/*
		$this->load->model('login/Slogin');
		*/
		$this->load->library('form_validation');
		$this->load->model('organisation/organisationmodel');
		$this->load->library('session');
		$this->load->model('route/Routemodel');
		$this->load->model('token/tokenmodel');
	}


	public function index_get()
	{
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{	
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				
					if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
					{
						$object['controller'] = $this;
						$this->load->view('route',$object);
					}
					else
					{
						redirect('dashboard','refresh');
					}
				
			}
			else
			{
				redirect('login','refresh');
			}
		}
		else
		{
				redirect('login','refresh');
		}

	}

	public function addroute_post()
	{
		
		$view = $this->input->post('view');

		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
					{
						$object['controller'] = $this;
						
						/* server side validation */
						$this->form_validation->set_rules('route', 'Route', 'required|callback_checkroute');
						$this->form_validation->set_rules('organisation', 'Organisation', 'required');
									if ($this->form_validation->run() == FALSE)
									{
										if($view!='')
										{
											$this->load->view('route',$object);
										}
										else
										{
											$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));
											$this->response($error);
										}
									}

						/* server side validation */
					}
					else
					{
						if(isset($view))
						{
							redirect('dashboard','refresh');
						}
						else
						{
							$result_set['status']='Failed';
							$result_set['message'] = "Permission denied";

							return $this->response($result_set);
						}
					}	
			}
			else
			{

				if(isset($view))
				{
					redirect('login','refresh');
				}
				else
				{
					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				}
			}
		}
		else
		{
			if(isset($view))
			{
				redirect('login','refresh');
			}
			else
			{
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);
			}

		}


		

	}

	/* get organisation list */
	public function getorglist_get($id=null)
	{
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{

				if($user_data['user_type_id']=='99' || $user_data['user_type_id']=='90')
				{
					$result = $this->organisationmodel->get_organisationlist();
				
					if($id!=null)
					{
						return $result;
					}
					else
					{
						
						$result_set['status']='success';
						$result_set['message'] = "";
						$result_set['organisation'] = $result;
						return $this->response($result_set);
						
					}	
				}
				else
				{	
					$result_set['status']='Failed';
					$result_set['message'] = "Permission denied";

					return $this->response($result_set);
				}

				
			}
			else
			{

					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				
			}
		}
		else
		{
			
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);

		}
				
	}


	public function checkroute()
		{
		/* setting up user id */
		$session_data = $this->session->userdata('usr_data'); /* retriving session data */
		$user_id = $session_data['user_id'];

			$route_name = $this->input->post('route');
			$description = $this->input->post('description');
			$organisation = $this->input->post('organisation');
			$view = $this->input->post('view');


			$data = array(
						'route_name'=>$route_name,
						'description'=>$description,
						'org_id'=>$organisation,
						'added_by'=>$user_id
						);

			/* checking product category already exist */

			$resultcnt = $this->Routemodel->getRoute($route_name,$organisation); 


			if($resultcnt)
			{

				if($view!='')
				{
					$this->form_validation->set_message('checkroute','Route already exist');
					return false;
				}
				else
				{
					$fnresult['status']='failed';
					$fnresult['message'] = "Route already exist";
					$this->response($fnresult);
				}
				
				
			}
			else
			{
				$result = $this->Routemodel->createRoute($data);
			
				if($result)
				{
					if($view!='')
					{
						$this->load->view('dashboard'); 
						return true;
					}

					$fnresult['status'] = "success";
					$fnresult['message'] = "New route added successfully";
					$fnresult['route_id'] = $result;
					$this->response($fnresult,OK);

				}
				else
				{	

					if($view!='')
					{
						$this->form_validation->set_message('checkroute','something goes wrong');
						return false;
					}
					else
					{
						$fnresult['status']=='failed';
						$fnresult['message'] = "New route addition failed";
						$this->response($fnresult,NOT_FOUND);
					}
				}
			}
		}
		
		public function addroute_get()
		{
			redirect('route','refresh');
		}
}
