<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Shop extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		/*
		$this->load->model('login/Slogin');
		*/
		$this->load->library('form_validation');
		$this->load->library('session');
		
	}


	public function index_get()
	{
		$this->load->view('shop');
	}

	public function addshop_post()
	{

		/* server side validation */
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|callback_checkuser');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
					if ($this->form_validation->run() == FALSE)
					{
						$this->response($arr);
						//$this->load->view('login');	
					}

		/* server side validation */

	}


	public function checkuser()
		{
			$username = $this->input->post('username');
			$passwd = $this->input->post('password');
			$view = $this->input->post('view');

			$data = array(
					   'username'=>$username,
						'passwd'=>$passwd,
						);

			$result = $this->Slogin->chklogin($data);

			
			
			if($result)
			{
				/* user type */
				$this->session->set_userdata('usr_type_id',$result['user_type_id']);

				
				if($view!='')
				{
					$this->load->view('dashboard'); 
					return true;
				}

				$result['status']='success';
				$result['message'] = "login success";

				$this->response($result);

			}
			else
			{
				if($view!='')
				{
					$this->form_validation->set_message('checkuser','username or password is incorrect');
					return false;
				}
				else
				{
					$result['status']='failed';
					$result['message'] = "login failed";
					$this->response($result);
				}
				
			}
		}
	
}
