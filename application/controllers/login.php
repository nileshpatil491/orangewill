<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Login extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->model('login/Slogin');
		$this->load->model('token/tokenmodel');
		$this->load->library('form_validation');
		$this->load->library('session');
	}


	public function index_get()
	{
		$this->load->view('login');
	}

	public function dologin_post()
	{

		$view = $this->input->post('view');

		/* server side validation */
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|numeric|callback_checkuser');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
					if ($this->form_validation->run() == FALSE)
					{

						if($view!='')
						{
							$this->load->view('login');
						}
						else
						{
							
							$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));

							$this->response($error,OK);
						}
					}

		/* server side validation */

	}


	public function checkuser()
		{
			$username = $this->input->post('username');
			$passwd = $this->input->post('password');
			$view = $this->input->post('view');

			$data = array(
					   'mob_no'=>$username,
						'passwd'=>$passwd,
						);

			$result = $this->Slogin->chklogin($data);

			if($result)
			{

				/* setting up token id */
				$token = md5(uniqid(rand(), TRUE));

				$token_result = $this->tokenmodel->create_token($username,$token);

				if($token_result)
				{
					$this->session->set_userdata('token',$token);

					/* user type */
					$this->session->set_userdata('usr_data',$result);

					if($view!='')
					{
						redirect('dashboard','refresh'); 
					}

					$fnresult['status']='success';
					$fnresult['message'] = "login success";
					$fnresult['users'] = array($result);
					
					$this->response($fnresult,OK);
					
				}
				else
				{
					$fnresult['status']='failed';
					$fnresult['message'] = "Token creation failed";
					
					$this->response($fnresult,OK);
				}


			}
			else
			{
				if($view!='')
				{
					$this->form_validation->set_message('checkuser','username or password is incorrect');
					return false;
				}
				else
				{
					$result['status']='failed';
					$result['message'] = "username or password is incorrect";
					$this->response($result,NOT_FOUND);
				}
				
			}
		}

		
		public function dologin_get()
		{
			redirect('login','refresh'); 
		}
	
}
