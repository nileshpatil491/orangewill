<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
//require APPPATH . '/libraries/Format.php';


class Organisation extends REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		
		$this->load->model('organisation/Organisationmodel');
		$this->load->model('token/tokenmodel');
		$this->load->library('form_validation');
		$this->load->library('session');
		
	}


	public function index_get()
	{

	$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				if($user_data['user_type_id']=='99')
				{
					$this->load->view('organisation');
				}
				else
				{
					redirect('dashboard','refresh');
				}
			}
			else
			{
				redirect('login','refresh');
			}
		}
		else
		{
				redirect('login','refresh');
		}
	}

	public function addorganisation_post()
	{
		$view = $this->input->post('view');
		$user_data = $this->session->userdata('usr_data');
		
		if(isset($user_data))
		{
			
			$sess_token = $this->session->userdata('token');

			$token_result = $this->tokenmodel->get_token();

			if($token_result)
			{
				if($user_data['user_type_id']=='99')
					{
						/* server side validation */
						$this->form_validation->set_rules('name', 'Organisation Name', 'required|callback_checkorganisation');
									if ($this->form_validation->run() == FALSE)
									{

										if($view!='')
										{
											$this->load->view('organisation');
										}
										else
										{
											$error = array('status'=>'failed','message'=>array($this->form_validation->error_array()));
											$this->response($error);
										}

										
										//$this->load->view('login');	
									}

						/* server side validation */
					}
					else
					{
						if(isset($view))
						{
							redirect('dashboard','refresh');
						}
						else
						{
							$result_set['status']='Failed';
							$result_set['message'] = "Permission denied";

							return $this->response($result_set);
						}
					}
			}
			else
			{

				if(isset($view))
				{
					redirect('login','refresh');
				}
				else
				{
					$result_set['status']='Failed';
					$result_set['message'] = "Invalid token";

					return $this->response($result_set);
				}
			}
			
		}
		else
		{
			if(isset($view))
			{
				redirect('login','refresh');
			}
			else
			{
				$result_set['status']='Failed';
				$result_set['message'] = "You are not logged in";

				return $this->response($result_set);
			}

		}


	}


	public function checkorganisation()
		{

			$session_data = $this->session->userdata('usr_data'); /* retriving session data */
			$user_id = $session_data['user_id'];
			$comp_id = $session_data['comp_id'];

			$organisation_name = $this->input->post('name');
			$organisation_description = $this->input->post('organisation_description');
			$view = $this->input->post('view');

			$data = array(
					   'org_name'=>$organisation_name,
						'org_description'=>$organisation_description,
						'added_by'=>$user_id,
						'comp_id'=>$comp_id
						);

			/* checking organisation already exist */

			$resultcnt = $this->Organisationmodel->getOrganisation($organisation_name,$comp_id); 

			if($resultcnt)
			{

				if($view!='')
				{
					$this->form_validation->set_message('checkorganisation','Organisation already exist');
					return false;
				}
				else
				{
					$fnresult['status']='failed';
					$fnresult['message'] = "Organisation already exist";
					$this->response($fnresult);
				}
				
				
			}
			else
			{
				$result = $this->Organisationmodel->createOrganisation($data);
            
				if($result)
				{
					
					//$this->form_validation->set_message('username','username or password is incorrect');
					
					if($view!='')
					{
						$this->load->view('dashboard'); 
						return true;
					}

					$fnresult['status'] = "success";
					$fnresult['message'] = "New Organisation added successfully";
					$fnresult['organisation_id'] = $result;
					$this->response($fnresult,OK);	
					
				}
				else
				{	

					if($view!='')
					{
						$this->form_validation->set_message('checkorganisation','something goes wrong');
						return false;
					}
					else
					{
						$fnresult['status']=='failed';
						$fnresult['message'] = "New Organisation addition failed";
						$this->response($fnresult,NOT_FOUND);
					}
				}
			}


		}

		public function addorganisation_get()
		{
			redirect('organisation','refresh');
		}
	
}
