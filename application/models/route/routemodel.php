<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Routemodel extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function createRoute($data)
	{

		$result = $this->db->insert('tbl_route', $data); 

		$route_id = $this->db->insert_id();

		return 	$route_id;		

	}

	public function getRoute($route_name,$org_id)
	{
		$this->db->select('route_id,route_name,org_id');

		$query = $this->db->get_where('tbl_route',array('route_name'=>$route_name,'org_id'=>$org_id,'is_active'=>'1'));
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function routelist($org_id)
	{
		$this->db->select('route_id,route_name,org_id');
		$this->db->where_in('org_id',$org_id);
		$query = $this->db->get_where('tbl_route',array('is_active'=>'1'))->result_array();

		return $query;
		
	}
    

}