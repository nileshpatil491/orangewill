<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tokenmodel extends CI_Model {

  

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function get_token()
	{
		$user_data = $this->session->userdata('usr_data');
		$user_id = $user_data['user_id'];
		$token = $this->session->userdata('token');

		$this->db->select('token_id,token');
		$result = $this->db->get_where('tbl_token',array('user_id' => $user_id,'token' => $token,'is_active' => '1'))->num_rows();
	
		return $result;

	}
    
	public function deactive_token()
	{
		$user_data = $this->session->userdata('usr_data');
		$user_id = $user_data['user_id'];
		$token = $this->session->userdata('token');
		
		$data = array(
               'is_active' => '0',
            );
		
		$this->db->where('user_id=',$user_id);
		$this->db->where('token=',$token);
		$this->db->where('is_active=','1');
		$this->db->update('tbl_token', $data);
		
		return $this->db->affected_rows();

	}
	
	public function create_token($username,$token)
	{
		/* fetching user id using mobile no */
		$this->db->where('mob_no=',$username);
		$user_id = $this->db->get('tbl_users')->row()->user_id;
	

		/* inserting token into the token table */
		$data_arr = array(
					'user_id'=>$user_id,
                    'token'=>$token
					 );

		$result = $this->db->insert('tbl_token', $data_arr); 

		return $this->db->affected_rows();
			
	}

}