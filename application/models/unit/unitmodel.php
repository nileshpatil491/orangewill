<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Unitmodel extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function get_unit()
	{

		$this->db->select('unit_id,unit_name');
		
		$result = $this->db->get_where('tbl_unit',array('is_active'=>'1'))->result_array();
	
		return $result;
	
	}

}