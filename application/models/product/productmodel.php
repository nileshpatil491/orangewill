<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Productmodel extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function createProdcategory($data)
	{

		$result = $this->db->insert('tbl_product_category', $data); 

		$prodcategory_id = $this->db->insert_id();

		return $prodcategory_id;		

	}

	public function createProduct($data)
	{

		$result = $this->db->insert('tbl_product', $data); 

		$prod_id = $this->db->insert_id();

		return $prod_id;		

	}

	public function getCategory($prodcat_name,$comp_id)
	{

		$this->db->select('prod_cat_name,comp_id');
		
		$query = $this->db->get_where('tbl_product_category',array('prod_cat_name' => $prodcat_name,'comp_id'=>$comp_id,'is_active'=>'1'));
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_prodcategory()
	{
		/* retriving company id for more fine search */
		$usr_data = $this->session->userdata('usr_data');
		$comp_id = $usr_data['comp_id'];

		$this->db->select('prod_cat_id,prod_cat_name');

		$result = $this->db->get_where('tbl_product_category',array('comp_id'=>$comp_id,'is_active'=>'1'))->result_array();
	
		return $result;
	
	}
    

}