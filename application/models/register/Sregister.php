<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Sregister extends CI_Model {

  

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function create($data)
	{
		/* creating company , ref id used in user table */
		
		$comp_data = array(
					'comp_name'=>$data['company']
					 );

		$result = $this->db->insert('tbl_company', $comp_data);
		$company_id = $this->db->insert_id();

		/* creating company , ref id used in user table */

		/* getting super admin id */

		//$this->db->select('user_type_id');
		$query = $this->db->get_where('tbl_user_type',array('user_type_name' => 'super_admin'))->row()->user_type_id;
		
		/* getting super admin id */

		$data_arr = array(
					'mob_no'=>$data['mob_no'],
                    'passwd'=>$data['passwd'],
					'comp_id'=>$company_id,
					'user_type_id'=>$query
					 );

		$result = $this->db->insert('tbl_users', $data_arr); 

		
		$user_id = $this->db->insert_id();

		return $user_id;

	}
    

}