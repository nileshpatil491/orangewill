<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Organisationmodel extends CI_Model {


    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function createOrganisation($data)
	{

		$result = $this->db->insert('tbl_organisation', $data); 

		$organisation_id = $this->db->insert_id();

		return 	$organisation_id;		

	}

	public function getOrganisation($organisation_name,$comp_id)
	{
		$this->db->select('org_name,comp_id');
		
		$query = $this->db->get_where('tbl_organisation', array('org_name' => $organisation_name,'comp_id' => $comp_id,'is_active'=>'1'));
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_organisationlist()
	{
		/* retriving company id for more fine search */
		$usr_data = $this->session->userdata('usr_data');
		$comp_id = $usr_data['comp_id'];

		$this->db->select('org_id,org_name,comp_id,org_description');

		$result = $this->db->get_where('tbl_organisation',array('comp_id'=>$comp_id,'is_active'=>'1'))->result_array();
	
		return $result;
	}
    

}