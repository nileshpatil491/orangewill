<?php 


class Usermodel extends CI_Model {

  

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	public function createuser($data)
	{
		/* company id 	
		
		$comp_id  = $this->session->userdata('usr_type_id[comp_id]');

		echo $comp_id;
		*/
	
		/* company id */
			
		$result = $this->db->insert('tbl_users', $data); 

		$user_id = $this->db->insert_id();

		return $user_id;

	}

	public function getMob($key)
	{
		$query = $this->db->get_where('tbl_users',array('mob_no'=>$key));
		
		if ($query->num_rows() > 0){
			return true;
		}
		else{
			return false;
		}
	}

	public function get_usertyp()
	{
		$this->db->select('user_type_id,user_type_name');
		$this->db->order_by("user_type_id", "desc");

		$result = $this->db->get_where('tbl_user_type',array('user_type_name !=' => 'super_admin'))->result_array();
	
		return $result;
	}
    

}