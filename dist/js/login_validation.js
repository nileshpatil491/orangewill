
$(function() {  
   // Setup form validation on the #register-form element
   $("form").validate({
   
	
       // Specify the validation rules
       rules: {
           username: {
               required: true,
			   number:true
           },
           password: {
               required: true,
               minlength: 5
           }
       },
       
       // Specify the validation error messages
       messages: {
           password: {
               required: "Please provide a password",
               minlength: "Your password must be at least 5 characters long"
           },
           username: "Please enter a valid username"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });

