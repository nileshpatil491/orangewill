$(function() {  


	$(".chosen-select").chosen({disable_search_threshold: 10});

   // Setup form validation on the #register-form element
   $("#form").validate({
   
	
       // Specify the validation rules
       rules: {
           name: {
               required: true
           },
		   mobile: {
               required: true,
			   minlength: 10,
			   number:true
			   
           },
           password: {
               required: true,
               minlength: 5
           },
			confpassword: {
               required: true,
			   minlength: 5,
               equalTo: '#password'
           },
			email:
		   {
				email:true
		   }

       },
       
       // Specify the validation error messages
       messages: {
		   mobile: {
               required: "Please provide a mobile no",
               minlength: "Your mobile no must be at least 10 nos long",
			   number:"please enter valid mobile no"
           },
           password: {
               required: "Please provide a password",
               minlength: "Your password must be at least 5 characters long"
           },
			
           name: "Please provide a name"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });


 });