
$(function() {  
   
   $(".chosen-select").chosen({disable_search_threshold: 10});


   $("#form").validate({
   
	
       // Specify the validation rules
       rules: {
           name: {
               required: true
           }
		   

       },
       
       // Specify the validation error messages
       messages: {
		   
           name: "Please provide a category name"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });