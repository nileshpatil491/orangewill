$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
	
       // Specify the validation rules
       rules: {
           name: {
               required: true
           },
		   unit_per_price: {
               required: true,
			   number:true
			   
           },
           qty_per_pack: {
               required: true,
			   number:true
           }

       },
       
       // Specify the validation error messages
       messages: {
		   unit_per_price: {
               required: "Please provide unit per price",
			   number:"please enter valid unit per price"
           },
           qty_per_pack: {
               required: "Please provide qty per pack",
			   number:"please enter valid qty per pack"
           },
			
           name: "Please provide a name"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });