$(function() {  
   // Setup form validation on the #register-form element
   $("form").validate({
   
	
       // Specify the validation rules
       rules: {
           route: {
               required: true
           }
       },
       
       // Specify the validation error messages
       messages: {
           route: "Please enter a route name"
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });