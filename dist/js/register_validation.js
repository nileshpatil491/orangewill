

$(function() {  
   // Setup form validation on the #register-form element
   $("#form").validate({
   
	
       // Specify the validation rules
       rules: {
		   mobile: {
               required: true,
			   number:true,
			   minlength: 10
			  
			   
           },
           password: {
               required: true,
               minlength: 5
           },
		   company:
		   {
				required: true
		   }

       },
       
       // Specify the validation error messages
       messages: {
		   mobile: {
               required: "Please provide a mobile no",
               minlength: "Your mobile no must be at least 10 nos long",
			   number:"please enter valid mobile no"
           },
           password: {
               required: "Please provide a password",
               minlength: "Your password must be at least 5 characters long"
           },
			company: {
               required: "Please provide a company name"
           }
},
       
       submitHandler: function(form) {

           form.submit();
       }
   });

 });
